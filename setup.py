from setuptools import setup, find_packages

setup(
    name = 'simpleadd',
    version = '0.1.0', 
    author = 'cs',
    author_email = 'chinmayshah3899@gmail.com',
    license = 'MIT',
    classifiesrs = [
        'Programming Language :: Python :: 3'
    ]
)